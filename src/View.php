<?php

namespace CustomerClient\App;

class View
{
    const TEMPLATE_DIR = APP_DIR . '/templates';

    private $template;

    public function __construct(string $template)
    {
        $this->template = $template;
    }

    public function render(array $data = []): string
    {
        ob_start();
        $this->includeTemplate($this->template, $data);
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    public static function includeTemplate(string $template, array $data = [])
    {
        extract($data);
        include self::TEMPLATE_DIR . '/' . ltrim($template, '/');
    }
}