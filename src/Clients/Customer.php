<?php

namespace CustomerClient\App\Clients;

use CustomerClient\App\Interfaces\Clients\CustomerApiInterface;

class Customer extends BaseClient implements CustomerApiInterface
{
    public function createApiKey(int $userId): array
    {
        return $this->get('/customer_api/create_key', ['userid' => $userId]);
    }

    public function createUser(
        string $name,
        string $status,
        string $referalTo = null
    ): array {
        return $this->post('/customers/create_user', [
            "name" => $name,
            "status" => $status,
            "referal_to" => $referalTo,
        ]);
    }
}