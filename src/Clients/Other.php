<?php

namespace CustomerClient\App\Clients;

use CustomerClient\App\Interfaces\Clients\OtherApiInterface;

class Other extends BaseClient implements OtherApiInterface
{
    public function countryExists(string $country)
    {
        return $this->get('/others/if_country_exists', [
            'country' => $country,
        ])['country_code'] ?? null;
    }

    public function cityExists(string $city): array
    {
        $city =  $this->get('/others/if_city_exists', [
            'city' => $city,
        ]);

        if (!isset($city['city_id'])) {
            return [];
        }

        return [
            'id' => $city['city_id'],
            'yandex_code' => $city['yandex_code'],
        ];
    }

    public function getCountryById(int $id)
    {
        return $this->get('/others/get_country_by_id', [
            'country_id' => $id,
        ])['response'] ?? null;
    }

    public function getCityById(int $id)
    {
        return $this->get('/others/get_city_by_id', [
            'city_id' => $id,
        ])['response'] ?? null;
    }
}