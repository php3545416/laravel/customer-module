<?php

namespace CustomerClient\App\Clients;

use CustomerClient\App\Interfaces\Clients\TaskApiInterface;

class Task extends BaseClient implements TaskApiInterface
{
    public function getTasksByUserId(int $userId, bool $ifActive = true): array
    {
        return $this->get('/tasks/get_user_tasks', [
            'userid' => $userId,
            'if_active' => (int) $ifActive,
            'nonactive_reason' => $ifActive ? 'Active' : 'Not active',
        ]);
    }

    public function create(int $userId, array $settings): array
    {
        return $this->post('/tasks/create', [
            'userid' => $userId,
            'settings_json' => $settings,
        ]);
    }

    public function getTaskInfo(int $taskId): array
    {
        return $this->get('/tasks/get_task_info', ['task_id' => $taskId]);
    }

    public function getTaskDaySum(int $taskId): array
    {
        return $this->get('/tasks/get_task_day_sum', ['task_id' => $taskId]);
    }

    public function getSubtaskTypes(): array
    {
        return $this->get('/tasks/get_subtasks_types')['subtasks_types'];
    }

    public function getSubtasks(): array
    {
        return $this->get('/tasks/get_subtasks')['subtasks'];
    }

    public function changeSettings(int $taskId, array $settings): array
    {
        return $this->post('/tasks/change_settings', [
            'task_id' => $taskId,
            'settings_json' => $settings,
        ]);
    }

    public function setStatus(
        bool $isActive,
        int $taskId,
        string $reason = ''
    ): array {
        return $this->post('/tasks/set_status', [
            'if_active' => (int) $isActive,
            'task_id' => $taskId,
            'nonactive_reason' => empty($reason) ? ((bool) $isActive ? 'Active' : 'Not active') : $reason,
        ]);
    }

    public function setReservedSum(
        int $taskId,
        int $reservedSum
    ): array {
        return $this->post('/tasks/set_reserved_sum', [
            'task_id' => $taskId,
            'reserved_sum' => $reservedSum,
        ]);
    }
}