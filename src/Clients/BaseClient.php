<?php

namespace CustomerClient\App\Clients;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;

class BaseClient
{
    /** @var Client */
    protected $client;

    public function __construct(
        string $baseUri
    ) {
        $this->setClient($baseUri);
    }

    protected function setClient(string $baseUri)
    {
        $this->client = new Client([
            'base_uri' => $baseUri,
        ]);
    }

    protected function get(string $url, array $data = []): array
    {
        return $this->decodeResponseBody($this->client->get($url, [
            RequestOptions::JSON => $data
        ]));
    }

    protected function post(string $url, array $data = []): array
    {
        return $this->decodeResponseBody($this->client->post($url, [
            RequestOptions::JSON => $data,
        ]));
    }

    protected function delete(string $url): array
    {
        return $this->decodeResponseBody($this->client->delete($url));
    }

    private function decodeResponseBody(ResponseInterface $response): array
    {
        return json_decode($response->getBody()->getContents(), true);
    }
}