<?php

namespace CustomerClient\App\Clients;

use CustomerClient\App\Interfaces\Clients\CustomerEnvApiInterface;

class CustomerEnv extends BaseClient implements CustomerEnvApiInterface
{
    public function getResults(int $userId, string $solution = 'Ready for moderation'): array
    {
        return $this->get('/customer_env/get_customer_tasks_results', [
            'userid' => $userId,
            'solution' => $solution,
        ])['results'] ?? [];
    }

    public function getResultsByTask(int $taskId, string $solution = 'Ready for moderation'): array
    {
        return $this->get('/customer_env/get_results_by_task', [
            'task_id' => $taskId,
            'solution' => $solution,
        ])['results'] ?? [];
    }
}