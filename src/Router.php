<?php

namespace CustomerClient\App;

use Closure;
use Symfony\Component\HttpFoundation\Request;

class Router
{
    /** @var array|Route[] */
    private $routes = [];

    public function register(string $url, string $method, array $callback)
    {
        $this->routes[] = new Route($url, $method, $callback);
    }

    public function run(Request $request)
    {
        foreach ($this->routes as $route) {
            if ($route->match($request)) {
                return $route->run($request);
            }
        }
    }

    public function get(string $url, array $callback)
    {
        $this->register($url, 'GET', $callback);
    }

    public function post(string $url, array $callback)
    {
        $this->register($url, 'POST', $callback);
    }

}
