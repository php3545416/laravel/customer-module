<?php

namespace CustomerClient\App\Interfaces\Services;

use Illuminate\Database\Eloquent\Model;

interface UserServiceInterface
{
    /**
     * @return Model|null
     */
    public function getUserByEmail(string $email);

    public function createUser(
        string $email,
        int $status = 1,
        int $referalTo = null
    ): Model;
}