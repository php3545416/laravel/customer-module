<?php

namespace CustomerClient\App\Interfaces\Services;

interface TaskServiceInterface
{
    public function getSubTasks(array $task = []): array;

    public function createTask(int $userId, array $data): array;

    public function getTaskById(int $taskId): array;

    public function updateTask(int $taskId, array $data): array;

    public function updateTaskStatus(int $taskId, int $status): array;
}