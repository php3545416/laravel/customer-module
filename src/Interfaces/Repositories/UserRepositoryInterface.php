<?php

namespace CustomerClient\App\Interfaces\Repositories;

use CustomerClient\App\Models\User;
use Illuminate\Database\Eloquent\Model;

interface UserRepositoryInterface
{
    public function create(array $data): Model;

    public function createOrUpdate(array $data): Model;

    public function firstOrCreate(array $data): Model;

    /**
     * @return Model|null
     */
    public function getByEmail(string $email);

    public function update(int $id, array $data): int;
}