<?php

namespace CustomerClient\App\Interfaces\Clients;

interface TaskApiInterface
{
    public function getTasksByUserId(int $userId, bool $ifActive = true): array;

    public function create(int $userId, array $settings): array;

    public function getTaskInfo(int $taskId): array;

    public function getTaskDaySum(int $taskId): array;

    public function getSubtaskTypes(): array;

    public function getSubtasks(): array;

    public function changeSettings(int $taskId, array $settings): array;

    public function setStatus(
        bool $isActive,
        int $taskId,
        string $reason = ''
    ): array;

    public function setReservedSum(
        int $taskId,
        int $reservedSum
    ): array;
}