<?php

namespace CustomerClient\App\Interfaces\Clients;

interface CustomerApiInterface
{
    public function createApiKey(int $userId): array;

    public function createUser(
        string $name,
        string $status,
        string $referalTo = null
    ): array;
}