<?php

namespace CustomerClient\App\Interfaces\Clients;

interface CustomerEnvApiInterface
{
    public function getResults(int $userId, string $solution = 'Ready for moderation'): array;

    public function getResultsByTask(int $taskId): array;
}