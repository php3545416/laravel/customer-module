<?php

namespace CustomerClient\App\Interfaces\Clients;

interface OtherApiInterface
{
    public function countryExists(string $country);

    public function cityExists(string $city): array;

    public function getCountryById(int $id);

    public function getCityById(int $id);
}