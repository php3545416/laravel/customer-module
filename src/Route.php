<?php

namespace CustomerClient\App;

use Symfony\Component\HttpFoundation\Request;

class Route
{
    public $url;
    public $method;
    private $callback;

    public function __construct(string $url, string $method, array $callback)
    {
        $this->url = $this->normalizeUrl($url);
        $this->method = strtoupper($method);
        $this->callback = $callback;
    }

    public function match(Request $request): bool
    {
        return preg_match(
            '/^' . str_replace(['*', '/'], ['[\w\-_]+', '\/'], $this->url) . '$/',
            $this->normalizeUrl($request->getPathInfo())
        ) && $this->method === $request->getMethod();
    }

    public function run(Request $request)
    {
        $urlParts   = explode('/', $request->getPathInfo());
        $routeParts = explode('/', $this->url);

        $params = ['request' => $request];

        foreach ($routeParts as $key => $part) {
            if ($part === '*') {
                $params[] = $urlParts[$key];
            }
        }

        $class = $this->callback[0];
        $method = $this->callback[1];

        $controller = container()->get($class);

        return call_user_func_array([$controller, $method], $params);
    }


    private function normalizeUrl(string $url): string
    {
        return '/' . trim($url, '/');
    }
}
