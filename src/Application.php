<?php

namespace CustomerClient\App;

use Illuminate\Database\Capsule\Manager as Capsule;
use Symfony\Component\HttpFoundation\Request;
use Throwable;

class Application
{
    private $router;
    private $database;

    public function __construct(
        Router $router,
        Capsule $database
    ) {
        $this->router = $router;
        $this->database = $database;
    }

    public function run(Request $request)
    {
        $this->database->addConnection(config()->get('database'));

        $this->database->setAsGlobal();
        $this->database->bootEloquent();

        return $this->router->run($request);
    }
}
