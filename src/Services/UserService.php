<?php

namespace CustomerClient\App\Services;

use CustomerClient\App\Interfaces\Clients\CustomerApiInterface;
use CustomerClient\App\Interfaces\Repositories\UserRepositoryInterface;
use CustomerClient\App\Interfaces\Services\UserServiceInterface;
use Illuminate\Database\Eloquent\Model;

class UserService implements UserServiceInterface
{
    /** @var CustomerApiInterface $customerClient */
    private $customerClient;

    /** @var UserRepositoryInterface $userRepository */
    private $userRepository;

    public function __construct()
    {
        $this->customerClient = container()->get(CustomerApiInterface::class);
        $this->userRepository = container()->get(UserRepositoryInterface::class);
    }

    /**
     * @return Model|null
     */
    public function getUserByEmail(string $email)
    {
        return $this->userRepository->getByEmail($email);
    }

    public function createUser(
        string $email,
        int $status = 1,
        int $referalTo = null
    ): Model {
        $user = $this->getUserByEmail($email);

        if ($user) {
            return $user;
        }

        $apiUserCreated = $this->customerClient->createUser(
            $email,
            $status,
            $referalTo
        );

        $apiKeyCreated = $this->customerClient->createApiKey($apiUserCreated['userid']);

        return $this->userRepository->create([
            'id' => $apiUserCreated['userid'],
            'email' => $email,
            'name' => $email,
            'api_key' => $apiKeyCreated['api_key'],
            'status' => $status,
        ]);
    }
}