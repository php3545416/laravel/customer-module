<?php

namespace CustomerClient\App\Services;

use CustomerClient\App\Interfaces\Clients\CustomerEnvApiInterface;
use CustomerClient\App\Interfaces\Clients\OtherApiInterface;
use CustomerClient\App\Interfaces\Clients\TaskApiInterface;
use CustomerClient\App\Interfaces\Services\TaskServiceInterface;

class TaskService implements TaskServiceInterface
{
    /** @var TaskApiInterface $taskApi */
    private $taskApi;

    /** @var CustomerEnvApiInterface $customerEnvApi */
    private $customerEnvApi;

    /** @var OtherApiInterface $customerEnvApi */
    private $otherApi;

    public function __construct()
    {
        $this->taskApi = container()->get(TaskApiInterface::class);
        $this->customerEnvApi = container()->get(CustomerEnvApiInterface::class);
        $this->otherApi = container()->get(OtherApiInterface::class);
    }

    public function getUserTasks(int $userId): array
    {
//        $allSolutions = count($this->customerEnvApi->getResults($userId, 'Accepted'));
//        $onModerations = count($this->customerEnvApi->getResults($userId));

        $activeTasks = $this->taskApi->getTasksByUserId($userId)['tasks'];
        $notActiveTasks = $this->taskApi->getTasksByUserId($userId, false)['tasks'];

        return array_map(function (array $task) {
            return $this->prepareTaskItem($task);
        }, array_merge($activeTasks, $notActiveTasks));
    }

    public function getSubTasks(array $task = []): array
    {
        $types = $this->taskApi->getSubtaskTypes();
        $tasks = $this->taskApi->getSubtasks();

        $data = [];
        foreach ($types as $type) {
            $data[$type['id']] = [
                'id' => $type['id'],
                'description' => $type['description'],
                'name' => $type['name'],
            ];
        }

        foreach ($tasks as $index => $subtask) {
            $subtask = $this->prepareSubtask($index, $subtask, $task);

            foreach ($types as $type) {
                if ($type['id'] == $subtask['subtask_type']) {
                    $data[$type['id']]['subtasks'][] = $subtask;
                }
            }
        }

        return $data;
    }

    public function createTask(int $userId, array $data): array
    {
        $creationData = $this->prepareCreationData($data);

        $createdTask = $this->taskApi->create($userId, $creationData);

        $taskId = $createdTask['task_id'];

        $this->taskApi->setStatus(false, $taskId, 'On moderation');
        $this->taskApi->setReservedSum($taskId, 0);

        return $createdTask;
    }

    public function getTaskById(int $taskId): array
    {
        $task = $this->taskApi->getTaskInfo($taskId)['task'] ?? null;

        if (empty($task)) {
            return [];
        }

        unset($task['country']);

        $taskSettings = json_decode($task['settings_json'], true);

        $countryId = $taskSettings['country'] ?? null;

        if ($countryId == 0) {
            $task['country'] = '';
        } else if (!is_null($countryId) && !is_string($countryId)) {
            $task['country'] = $this->otherApi->getCountryById($countryId);
        }

        $cityId = $taskSettings['city'] ?? null;

        if ($cityId == 0) {
            $task['city'] = '';
        } else if (!is_null($cityId) && !is_string($cityId) ) {
            $task['city'] = $this->otherApi->getCityById($cityId);
        }

        return array_merge($taskSettings, $task);
    }

    public function updateTask(int $taskId, array $data): array
    {
        return $this->taskApi->changeSettings($taskId, $this->prepareCreationData($data));
    }

    public function updateTaskStatus(int $taskId, int $status): array
    {
        return $this->taskApi->setStatus($status, $taskId);
    }

    public function getCustomerTasksResults(int $userid): array
    {
        return $this->customerEnvApi->getResults($userid)['results'];
    }

    private function prepareSubtask(int $index, array $subTask, array $task = []): array
    {
        return [
            'id' => $index,
            'type' => 'checkbox',
            'input_name' => 'subtask_' . $subTask['id'],
            'name' => $subTask['name'],
            'user_text' => $subTask['user_text'],
            'subtask_type' => $subTask['subtask_type'],
            'customer_description' => $subTask['customer_description'],
            'value' => in_array($subTask['id'], $task['subtasks'] ?? []) ? 'checked' : '',
        ];
    }

    private function prepareCreationData(array $data): array
    {
        unset($data['email']);

        if (!empty($data['country'])) {
            $countryCode = $this->otherApi->countryExists($data['country']);

            if (!is_null($countryCode)) {
                $data['country'] = $countryCode;
            } else {
                $data['country'] = 0;
            }
        } else {
            $data['country'] = 0;
        }

        if (!empty($data['city'])) {
            $city = $this->otherApi->cityExists($data['city']);

            if (!empty($city)) {
                $data['city'] = $city['id'];
                $data['yandex_code'] = $city['yandex_code'];
            } else {
                $data['city'] = 0;
                $data['yandex_code'] = 0;
            }
        } else {
            $data['city'] = 0;
            $data['yandex_code'] = 0;
        }

        $taskTasksIndices = [];
        foreach ($data as $key => $value) {
            if (str_contains($key, 'subtask')) {
                $taskTasksIndices[] = (int) str_after($key, 'subtask_');
                unset($data[$key]);
            }
        }

        $data = array_merge($data, ['url' => $data['url']]);
        $data = array_merge($data, ['subtasks' => $taskTasksIndices]);

        $data['site_index'] = 0;
        if (isset($data['site_index'])) {
            $data = array_merge($data, ['site_index' => $data['site_index']]);
        }

        if (isset($data['key_phrases'])) {
            $phrases = [];
            foreach ($data['key_phrases'] as $key => $phrase) {
                foreach ($phrase as $subkey => $value) {
                    if ($key == 0 && $subkey == 0) {
                        $val = $value == 'on' ? '-' : $value;
                    } else if ($key == 0 && $subkey == 2 && $value == 0) {
                        $val = 1;
                    } else {
                        $val = intval($value);
                    }

                    $phrases[$key][$subkey] = $val;
                }
            }
            $data = array_merge($data, ['key_phrases' => $phrases]);
        }

        $data['transition_from_searchEngine'] = 1;
        if (isset($data['transition_from_searchEngine'])) {
            $data = array_merge($data, [
                'transition_from_searchEngine' => $data['transition_from_searchEngine'],
            ]);
        }

        foreach ($data as $key => $value) {
            if (str_contains($key, 'additional') && $value == "") {
                unset($data[$key]);
            }
        }

        return $data;
    }

    private function prepareTaskItem(array $task): array
    {
        $settings = $task['settings_json'];

        $allSolutions = count($this->customerEnvApi->getResultsByTask($task['id'], 'Accepted'));
        $onModerations = count($this->customerEnvApi->getResultsByTask($task['id']));

        $checked = false;
        if ($task['if_active'] && $task['nonactive_reason'] == 'Active') {
            $checked = true;
        }

        if (!$task['if_active'] && $task['nonactive_reason'] == 'Not active') {
            $checked = false;
        }

        $pause = false;
        if ($task['nonactive_reason'] == 'Not active') {
            $pause = true;
        }

        return [
            'id' => $task['id'],
            'url' => $settings['url'],
            'limit' => $settings['key_phrases'][0][2] ?? 0,
            'reserved_sum' => $task['reserved_sum'],
            'solutions' => $allSolutions,
            'on_moderation' => $onModerations,
            'sum_per_day' => $this->taskApi->getTaskDaySum($task['id'])['day_sum'],
            'nonactive_reason' => $task['nonactive_reason'],
            'if_active' => $task['if_active'],
            'pause' => $pause,
            'checked' => $checked ? 'checked' : '',
            'disabled' => !in_array($task['nonactive_reason'], ['Active', 'Not active']) ? 'disabled' : '',
            'customer_id' => $task['customer_id'],
            'action' => url('/tasks/' . $task['id'] . '/updateStatus'),
        ];
    }
}