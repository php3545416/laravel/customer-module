<?php

namespace CustomerClient\App\Controllers;

use CustomerClient\App\Interfaces\Services\TaskServiceInterface;
use CustomerClient\App\Interfaces\Services\UserServiceInterface;
use CustomerClient\App\View;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Throwable;

class PagesController
{
    /** @var UserServiceInterface $customerClient */
    private $userService;

    /** @var TaskServiceInterface $customerClient */
    private $taskService;

    public function __construct()
    {
        $this->userService = container()->get(UserServiceInterface::class);
        $this->taskService = container()->get(TaskServiceInterface::class);
    }

    /**
     * @throws \Exception
     */
    public function index(Request $request): string
    {
        $email = $request->query->get('email');

        if (!$email) {
            return (new View('/pages/incorrect-url.php'))->render();
        }

        $user = $this->userService->getUserByEmail($request->query->get('email'));

        if (!$user) {
            return (new View('/pages/tasks.php'))->render(['tasks' => []]);
//            return (new View('/pages/user-not-found.php'))->render();
        }

        $tasks = $this->taskService->getUserTasks($user->id);

        if (empty($tasks)) {
            return (new View('/pages/empty-tasks.php'))->render();
        }

        return (new View('/pages/tasks.php'))->render(['tasks' => $tasks]);
    }

    public function updateTaskStatus(Request $request, $taskId)
    {
        $email = $request->request->get('email');
        $this->taskService->updateTaskStatus($taskId, !!$request->request->get('status'));

        return RedirectResponse::create(url('/tasks')."?email={$email}")->send();
    }

    public function create(): string
    {
        $subTasks = $this->taskService->getSubTasks();
        return (new View('/pages/task-create.php'))->render(['tasks' => $subTasks]);
    }

    public function store(Request $request)
    {
        $params = $request->request->all();

        if (empty($params['email'])) {
            return (new View('/pages/incorrect-url.php'))->render();
        }

        $user = $this->userService->createUser($params['email']);

        $createdTask = $this->taskService->createTask($user->id, $params);

        return RedirectResponse::create(url('/tasks')."?email={$params['email']}")->send();
    }

    public function edit(Request $request, $taskId)
    {
        $email = $request->query->get('email');

        if (empty($email)) {
            return (new View('/pages/incorrect-url.php'))->render();
        }

        $task = $this->taskService->getTaskById($taskId);

        if (empty($task)) {
            return RedirectResponse::create(url('/tasks?email=' . $email))->send();
        }

        $tasks = $this->taskService->getSubTasks($task);

        return (new View('/pages/task-edit.php'))->render(array_merge(['currentTask' => $task], ['tasks' => $tasks]));
    }

    public function update(Request $request, $taskId): string
    {
        $params = $request->request->all();

        if (empty($params['email'])) {
            return (new View('/pages/incorrect-url.php'))->render();
        }

        $this->taskService->updateTask($taskId, $request->request->all());

        return RedirectResponse::create(url('/tasks')."?email={$params['email']}")->send();
    }
}
