<?php

namespace CustomerClient\App\Repositories;

use CustomerClient\App\Interfaces\Repositories\UserRepositoryInterface;
use CustomerClient\App\Models\User;
use Illuminate\Database\Eloquent\Model;

class UserRepository implements UserRepositoryInterface
{
    private function getModel(): User
    {
        return new User();
    }

    public function create(array $data): Model
    {
        return $this->getModel()->newQuery()->create($data);
    }

    public function createOrUpdate(array $data): Model
    {
        return $this->getModel()->newQuery()->updateOrCreate([
            'email' => $data['email']
        ], $data);
    }

    public function firstOrCreate(array $data): Model
    {
        return $this->getModel()->newQuery()->firstOrCreate(
            ['email' => $data['email']],
            $data
        );
    }

    public function getByEmail(string $email)
    {
        return $this->getModel()->newQuery()->where('email', $email)->first();
    }

    public function update(int $id, array $data): int
    {
        return $this->getModel()->newQuery()->where('id', $id)->update($data);
    }
}