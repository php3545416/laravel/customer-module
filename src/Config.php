<?php

namespace CustomerClient\App;

class Config
{
    private $config = [];
    private $configDir;

    public function __construct(string $configDir)
    {
        $this->configDir = $configDir;
    }

    public function load(): Config
    {
        $files = scandir($this->configDir);

        foreach ($files as $file) {
            $filePath = $this->configDir . DIRECTORY_SEPARATOR . $file;

            if (!is_file($filePath)) {
                continue;
            }

            $fileInfo = pathinfo($filePath);

            if ($fileInfo['extension'] !== 'php') {
                continue;
            }

            $this->config[$fileInfo['filename']] = include($filePath);
        }

        return $this;
    }

    public function get(string $key, $default = null)
    {
        $data = $this->config;

        foreach (explode('.', $key) as $keyPart) {
            if (! isset($data[$keyPart])) {
                return $default;
            }

            $data = $data[$keyPart];
        }

        return $data;
    }
}

