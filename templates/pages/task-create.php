<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Interface</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 20px;
        }
        span {
            padding-top:10px;
            padding-bottom:10px;
            margin: 20px;
            line-height: 10px;
            color:#a7a7a7;

        }
        .main-form {
            background: #fff;
            padding: 20px;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }
        .section {
            margin-bottom: 20px;
        }
        h2, h3 {
            border-bottom: 2px solid #ccc;
            padding-bottom: 5px;
            margin-bottom: 10px;
        }
        label {
            display: block;
            margin-bottom: 5px;
        }
        input[type="text"], input[type="url"], input[type="number"] {
            width: calc(100% - 22px);
            padding: 10px;
            margin-top: 5px;
            margin-bottom: 10px;
            border: 1px solid #ccc;
        }
    </style>

</head>
<body>
<a href="<?=url('/tasks?email=' . $_GET['email'] ?? '')?>">Вернуться к списку заказов</a>

<form action="<?=url('/task')?>" method="POST" class="main-form">
    <input type="hidden" name="email" value="<?=($_GET['email'] ?? null) ?>">

    <div class="section">
        <label for="url">Адрес страницы (URL):</label>
        <span class="ask"> � </span><span>В это поле можно ввести домен, адрес конкретной страницы, поста, и т.п.</span>
        <input id="url" required type="text" name="url" placeholder="https://site.ru">
    </div>

    <div class="section">
        <h3>Живой трафик (фото отчет о каждом посещении) + Доп. задачи</h3>
    </div>

    <div class="section">
        <label>
            <input type="hidden" name="key_phrases[0][0]" value="-" >
            <input type="checkbox" name="key_phrases[0][0]"> Прямые переходы
            <span class="ask"> � </span> <span>Исполнитель будет переходить по прямой ссылке сразу на ваш ресурс</span>
        </label>

        <input type="hidden" name="key_phrases[0][1]" value="0">

        <div>
            <label for="search-keywords-limit">Лимит в сутки</label>
            <input type="text" required id="search-keywords-limit" name="key_phrases[0][2]">
        </div>

        <input type="hidden" name="key_phrases[0][3]" value="0">
    </div>

    <?php foreach ($tasks as $task) { ?>
        <div class="section">
            <h3><b><?=$task['name']?><span class="ask"> � </span> <span><?=$task['description']?></span></b></h3>

            <?php
            $found = true;
            foreach($task['subtasks'] as $subTask) {?>
                <?php if ($task['name'] == 'Соцсети' && $found) { ?>
                    <input type="text" placeholder="Выбор" name="<?='additional_link_'.$subTask['id']?>" value="<?=$currentTask['additional_link_' . $subTask['id']] ?? ''?>" />
                    <?php
                    $found = false;
                } ?>

                <label for="<?=$subTask['input_name']?>">
                    <input type="<?=$subTask['type']?>" name="<?=$subTask['input_name']?>" id="<?=$subTask['input_name']?>">
                    <?=$subTask['name']?> <span class="ask" > � </span> <span><?=$subTask['customer_description']?></span>
                </label>

                <?php if ($subTask['input_name'] == 'subtask_3') {?>
                    <input type="text" placeholder="Текст для ввода в чат" name="<?='additional_link_'.$subTask['id']?>" />
                <?php } ?>
            <?php } ?>
        </div>
    <?php } ?>

    <div class="section">
        <h3>Геограция</h3>


        <label for="country">Страна</label>
        <span class="ask" > � </span> <span>Укажите исполнителям из какой страны давать задания в первую очередь</span>
        <input type="text" id="country" name="country" placeholder="Страна">

        <label for="country">Город</label>
        <span class="ask" > � </span> <span>Укажите исполнителям из какого города давать задания в первую очередь</span>
        <input type="text" id="city" name="city" placeholder="Город">
    </div>

    <button type="submit">Создать</button>
</form>
</body>
</html>

<!--    <div class="section">-->
<!--        <input type="checkbox" id="alt-url-checkbox">-->
<!--        <label for="alt-url-checkbox">Альтернативные URL:</label>-->
<!--        <input type="url" id="alt-url" name="alt-url" placeholder="https://site.ru">-->
<!--        <button type="button">+</button>-->
<!--    </div>-->
<!--        <label>-->
<!--            <input type="checkbox" name="tasks" value="search-engines-domains"> Из Поисковых систем (поиск по <strong>ссылке/домену</strong>)-->
<!--        </label>-->
<!--        <span class="ask" > � </span> <span>Сайт (ссылка) должны быть проиндексированы в ПС</span>-->
<!--        <div class="limits">-->
<!--            <label for="limit-from">Лимит в сутки, от</label>-->
<!--            <input type="number" id="limit-from" name="limit-from">-->
<!--            <label for="limit-to">до</label>-->
<!--            <input type="number" id="limit-to" name="limit-to">-->
<!--        </div>-->
<!--        <label>-->
<!--            <input type="checkbox" name="tasks" value="search-engines-keywords"> Из Поисковых систем (поиск по <strong>ключевым словам</strong>)-->
<!--        </label>-->
<!--        <span class="ask" > � </span> <span>Сайт (ссылка) должны быть проиндексированы в ПС</span>-->
<!--        <div class="limits">-->
<!--            <label for="limit-from-2">Лимит в сутки, от</label>-->
<!--            <input type="number" id="limit-from-2" name="limit-from-2">-->
<!--            <label for="limit-to-2">до</label>-->
<!--            <input type="number" id="limit-to-2" name="limit-to-2">-->
<!--        </div>-->
<!--        <label>-->
<!--            <input type="checkbox" name="tasks" value="image-search"> Поиск по картинке-->
<!--        </label>-->
<!--        <div class="limits">-->
<!--            <label for="limit-from-3">Лимит в сутки, от</label>-->
<!--            <input type="number" id="limit-from-3" name="limit-from-3">-->
<!--            <label for="limit-to-3">до</label>-->
<!--            <input type="number" id="limit-to-3" name="limit-to-3">-->
<!--        </div>-->