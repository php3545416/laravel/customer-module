<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Orders</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f9f9f9;
            margin: 0;
            padding: 550px;
            justify-content: center;
            align-items: center;
            height: 100vh;
        }
        .status {
            display: flex;
            align-items: center;
            justify-content: center;
            border: 2px solid #e74c3c;
            border-radius: 15px;
            width: 100px;
            padding: 10px;
            color: white;
            background-color: #e74c3c;
            cursor: pointer;
        }
        .container {
            background-color: white;
            border: 1px solid #ccc;
            border-radius: 10px;
            padding: 20px;
            width: 90%;
            max-width: 800px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            box-sizing: border-box;
            display: flex;
            justify-content: space-between;
            flex-wrap: wrap;
        }
        .header {
            width: 100%;
            margin-bottom: 20px;
            display: flex;
            justify-content: space-between;
            align-items: center;
        }
        .header .url {
            color: #007bff;
            text-decoration: none;
            font-weight: bold;
        }
        .column {
            display: flex;
            flex-direction: column;
            align-items: center;
            margin-bottom: 20px;
            flex: 1;
            min-width: 120px;
        }
        .column div {
            margin: 5px 0;
            text-align: center;
        }
        .status {
            display: flex;
            align-items: center;
        }
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }
        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }
        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            transition: 0.4s;
            border-radius: 34px;
        }
        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            transition: 0.4s;
            border-radius: 50%;
        }
        input:checked + .slider {
            background-color: #2196F3;
        }
        input:checked + .slider:before {
            transform: translateX(26px);
        }
        @media (max-width: 600px) {
            .container {
                padding: 10px;
            }
            .column {
                margin-bottom: 10px;
                min-width: 100%;
            }
        }
    </style>
</head>
<body>
    <a href="<?=url('/task/create?email=' . $_GET['email'] ?? null)?>">Создать заказ</a>

    <?php foreach ($tasks as $task) { ?>
        <div class="container">
            <div class="header">
                <a href="<?=$task['url']?>" class="url"><?=$task['url']?></a>

                <?php if ($task['pause']) { ?>
                    <div class="status"><span>На паузе</span></div>
                <?php } ?>

                    <form method="POST" action="<?=$task['action']?>">
                        <input type="hidden" name="email" value="<?=$_GET['email'] ?? null?>">
                        <label class="switch">
                            <input onchange="this.form.submit()" type="checkbox" name="status" <?=$task['disabled']?> <?=$task['checked']?>>
                        <span class="slider"></span>
                        </label>
                    </form>
            </div>

            <div class="column">
                <div>Счет заказа</div>
                <div><?=$task['reserved_sum']?></div>
            </div>

            <div class="column">
                <div>Стоимость</div>
                <div><?=$task['sum_per_day'] ?? null ?></div>
            </div>

            <div class="column">
                <div>Лимит в день</div>
                <div><?=$task['limit']?></div>
            </div>

            <div class="column">
                <div>На проверке</div>
                <div><?=$task['on_moderation']?></div>
            </div>

            <div class="column">
                <div>Всего выполнений</div>
                <div><?=$task['solutions']?></div>
            </div>

            <a href="<?=url('/task/edit/' .$task['id'] . '?email=' . $_GET['email'] ?? null)?>">Изменить</a>
        </div>
    <?php }?>

    <?php if (empty($tasks)) {?>
        <div class="container">
            У текущего пользователя нет созданных заказов
        </div>
    <?php } ?>
</body>
</html>
