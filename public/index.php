<?php

error_reporting(E_ALL);
ini_set('display_errors', true);

define('APP_DIR', dirname(__DIR__));
require_once dirname(__DIR__) . '/vendor/autoload.php';
require_once APP_DIR . '/bootstrap.php';

use Illuminate\Database\Capsule\Manager as Capsule;
use CustomerClient\App\Application;
use CustomerClient\App\Controllers\PagesController;
use CustomerClient\App\Router;
use Symfony\Component\HttpFoundation\Request;

$router = new Router();

$router->get('/tasks', [PagesController::class, 'index']);
$router->post('/tasks/*/updateStatus', [PagesController::class, 'updateTaskStatus']);
$router->get('/task/create', [PagesController::class, 'create']);
$router->post('/task', [PagesController::class, 'store']);
$router->get('/task/edit/*', [PagesController::class, 'edit']);
$router->post('/task/edit/*', [PagesController::class, 'update']);

$application = new Application($router, new Capsule());

$response = $application->run(Request::createFromGlobals());

echo $response;
