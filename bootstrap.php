<?php

use CustomerClient\App\Clients\Customer;
use CustomerClient\App\Clients\CustomerEnv;
use CustomerClient\App\Clients\Other;
use CustomerClient\App\Clients\Task;
use CustomerClient\App\Config;
use CustomerClient\App\Controllers\PagesController;
use CustomerClient\App\Interfaces\Clients\CustomerApiInterface;
use CustomerClient\App\Interfaces\Clients\CustomerEnvApiInterface;
use CustomerClient\App\Interfaces\Clients\OtherApiInterface;
use CustomerClient\App\Interfaces\Clients\TaskApiInterface;
use CustomerClient\App\Interfaces\Repositories\UserRepositoryInterface;
use CustomerClient\App\Interfaces\Services\TaskServiceInterface;
use CustomerClient\App\Interfaces\Services\UserServiceInterface;
use CustomerClient\App\Repositories\UserRepository;
use CustomerClient\App\Services\TaskService;
use CustomerClient\App\Services\UserService;
use Dotenv\Dotenv;
use Illuminate\Container\Container;

function container(): Container
{
    return Container::getInstance();
}

function config(): Config
{
    return container()->get(Config::class);
}

function url(string $suffix = ''): string
{
    return config()->get('app.app_url') . $suffix;
}

$dotenv = Dotenv::createImmutable(APP_DIR);
$dotenv->load();

container()->singleton(Config::class, function () {
    return (new Config(APP_DIR . '/config'))->load();
});

container()->singleton(UserRepositoryInterface::class, UserRepository::class);

container()->singleton(UserServiceInterface::class, UserService::class);

container()->singleton(CustomerApiInterface::class, function () {
    return container()->make(
        Customer::class, ['baseUri' => config()->get('services.customer_base_api_uri')]
    );
});

container()->singleton(TaskApiInterface::class, function () {
    return container()->make(
        Task::class, ['baseUri' => config()->get('services.customer_base_api_uri')]
    );
});

container()->singleton(CustomerEnvApiInterface::class, function () {
    return container()->make(
        CustomerEnv::class, ['baseUri' => config()->get('services.customer_base_api_uri')]
    );
});

container()->singleton(OtherApiInterface::class, function () {
    return container()->make(
        Other::class, ['baseUri' => config()->get('services.customer_base_api_uri')]
    );
});

container()->singleton(TaskServiceInterface::class, TaskService::class);

container()->singleton(PagesController::class, function () {
   return new PagesController();
});